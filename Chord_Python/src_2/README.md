Better implementation of the Chord framework. This one, rather than thinking about opening communication channels between Nodes, is about sending RPCs to the nodes instead (which is what actually happens). This means that instead of having the nodes wait for a return value, it will continue going about it's business until an RPC containing the return value comes, after which it will process it. Here is how the RPC is formatted.


There are two types of RPCs: value RPCs, which contain return values, and function RPCs, which make the Node call a function. 

value RPC:
    formatted as follows: RPC = ('value', (var_name, val))
    var_name denotes where the returned value should be placed, while val denotes the actual returned value.

function RPC:
    formatted as follows: RPC = ('function', (func_name, kwargs))
    func_name is the string denoting the function, and kwargs are the arguments for the function.


The Chord Ring works as follows:
    To move one step forward in time, every node pops off an RPC from it's queue (incoming_RPCs), and runs it. Because this is done serially in numerical order, multiple steps in find_successor can end up being run in one step forward in time. This also applies to everything else. Regardless, this is a close approximation.

Currently:
    The method mostly works, with small amounts of finger table values that are wrong. Not sure why this issue happens. This is almost definitely a bug, since some of the finger tables are None, meaning fix_finger never ends up finding the correct successor.

To do:
    Figure out why the bug is happening. I flagged two suspicious parts in the code.
    Incorporate spontaneous node failure, in other words, Section E.3 of the paper into the implementation.


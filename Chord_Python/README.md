A serial implementation of Chord in Python. 
All operations are defined in terms of smaller atomic operations. 
Each node contains a queue of these atomic operations. 
We can then go through the nodes in a round robin fashion to execute for pseudoparallelism.

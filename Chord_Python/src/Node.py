import constants as c
import math
import random

class ChordNode:
    def __init__(self, ID):
        self.ID           = ID
        self.pred_ID      = None
        self.finger_table = [None]*c.ring_size
        self.storage      = {} # contains key/value pairs
        self.func_q       = [] # Queue of functions to execute
        self.open_channel = -1 # if not -1, then Node is waiting. -1 signifies a "closed" channel
        self.buff         = None # buffer that stores messages passed from other nodes.
        self.offset       = random.randint(0, c.start_offset)
        self.op_count     = 0 #random.randint(0, c.start_offset) # counter that is like a clock
        

    '''
    Function that passes along messages along the open channel. Either
    the node's channel points to the node itself, in which the value then gets 
    stored in the buffer, otherwise it passes the value along the channel.
    After the value is passed along, the channel is no longer used, so it gets 
    "closed".
    Note that this is a function that can run when open_channel is not -1, since
    it should ONLY be run if there is an open channel and node is waiting.
    '''
    def return_val(self, val, nodeDict):
        # If channel points to itself, write the incoming value to buffer.
        if self.open_channel == self.ID:
            self.buff = val
        # If channel points to someone else, send incoming value through the channel
        # and have them continue to send it.
        else:
            caller = nodeDict[self.open_channel]
            kwargs = {'val':val, 'nodeDict':nodeDict}
            caller.func_q.insert(0, (caller.return_val, kwargs))
        # "Close" the channel
        self.open_channel = -1


    '''
    Function that finds successor. It either finds the successor, in which it will
    initiate the return_val chain, or it will continue the find_successor chain
    and wait for the return value.
    '''
    def find_successor(self, caller_ID, key, nodeDict):
        # Open channel with node that you should return value to.
        self.open_channel = caller_ID 

        # If we've found the successor, return it.
        if between(self.ID, self.finger_table[0], key):
            #caller = nodeDict[self.open_channel]     
            #self.open_channel = -1
            print('Successor found to be {}. Returning Value.'.format(self.finger_table[0]))
            kwargs = {'val':self.finger_table[0], 'nodeDict':nodeDict}
            self.func_q.insert(0, (self.return_val, kwargs))
            #caller.func_q.insert(0, (caller.return_val, kwargs))
        # Otherwise, find the relevant finger table entry, and have them search.
        else:
            for i in range(c.ring_size-1, -1, -1):
                print(i)
                if self.finger_table[i] is not None and between(self.ID, self.finger_table[i], key):
                    next_node = nodeDict[self.finger_table[i]]
                    kwargs = {'caller_ID': self.ID, 'key': key, 'nodeDict':nodeDict}
                    next_node.func_q.append((next_node.find_successor, kwargs))
                    return
            kwargs = {'val':self.ID, 'nodeDict':nodeDict}
            self.func_q.insert(0, (self.return_val, kwargs))

    '''
    Initialize when there are no other nodes
    '''
    def create(self):
        self.pred_ID = None
        self.finger_table[0] = self.ID


    '''
    components of joining the Chord ring. First, starting at the joinID,
    we find the successor of the node. Then, we set successor accordingly.
    '''
    def join_1(self, joinID, nodeDict):
        start_node = nodeDict[joinID]
        self.open_channel = self.ID
        kwargs = {'caller_ID': self.ID, 'key': self.ID, 'nodeDict': nodeDict}
        start_node.func_q.append((start_node.find_successor, kwargs))
    def join_2(self):
        print("Finishing join for node {}: Successor is {}".format(self.ID, self.buff))
        self.finger_table[0] = self.buff
        self.buff = None
    '''
    the third step to joining the Chord ring. Once the successor is established,
    the node must contact the successor to relinquish relevant keys.
    '''
    def receive_items(self, nodeDict):
        # Asks successor to send items.
        succ_node = nodeDict[self.finger_table[0]]
        if succ_node.ID == self.ID:
            print("Node {} has same ID as Node {}. No longer receiving items.".format(self.ID, succ_node.ID))
            return
        print("Node {} prepped to receive items from Node {}".format(self.ID, succ_node.ID))
        self.open_channel = self.ID
        kwargs = {'caller_ID':self.ID, 'nodeDict':nodeDict}
        succ_node.func_q.append((succ_node.send_items, kwargs))


    '''
    Command given by another node to send them all items that are less than
    or equal to the provided key. It doesn't do it all at once, but rather
    sends it in batches (to better emulate streaming). 
    Sending is done by directly modifying the storage of receiving node. 
    We then return None to the receiving node to let them know sending
    is over. The return function is added first since commands are added
    in a stack fashion (FILO).
    '''
    def send_items(self, caller_ID, nodeDict):
        self.open_channel = caller_ID # Opens a channel with receiver Node
        # Get a list of items to send to receiver.
        send_list = [] 
        for k, v in self.storage.items():
            if not between(caller_ID, self.ID, k):
                send_list.append((k, v))
        num_batch = math.ceil(len(send_list)/c.batch_size)
        # The following gets added onto the FRONT of the queue: all send_batch
        # operations, and then return None to let the receiver know that we 
        # are finished and to reset the open channel.
        kwargs = {'val': None, 'nodeDict':nodeDict}
        self.func_q.insert(0, (self.return_val, kwargs))
        for i in range(num_batch):
            batch = send_list[i*c.batch_size:(i+1)*c.batch_size]
            kwargs = {'items': batch, 'nodeDict': nodeDict}
            self.func_q.insert(0, (self.send_batch, kwargs))
    def send_batch(self, items, nodeDict):
        caller = nodeDict[self.open_channel]
        for item in items:
            caller.storage[item[0]] = item[1]
            self.storage.pop(item[0])
        

    '''
    Helper function to stabilize. Passes the ID of predecessor to the
    one who made the query.
    '''
    def query_pred(self, caller_ID, nodeDict):
        self.open_channel = caller_ID
        self.return_val(self.pred_ID, nodeDict)
    '''
    components of stabilizing the node: finding the successor. 
    It first gets the predecessor of the successor, 
    and then updates the successor value in the finger table. 
    Finally, it notifies the successor.
    '''
    def stabilize_1(self, nodeDict):
        successor = nodeDict[self.finger_table[0]]
        print('successor {} ID {}'.format(successor.ID, self.ID))
        if successor.ID == self.ID:
            print('Returning')
            return 
        self.open_channel = self.ID
        kwargs = {'caller_ID': self.ID, 'nodeDict': nodeDict} 
        successor.func_q.append((successor.query_pred, kwargs))
    def stabilize_2(self, nodeDict):
        succ_pred = self.buff
        self.buff = None
        if succ_pred is not None and between(self.ID, self.finger_table[0], succ_pred):
            self.finger_table[0] = succ_pred
        succ = nodeDict[self.finger_table[0]] 
        if succ.ID == self.ID:
            return
        kwargs = {'pot_pred_ID': self.ID}
        succ.func_q.append((succ.notify, kwargs))

    
    '''
    Checks if the passed in node ID is the valid predecessor. If so, update.
    '''
    def notify(self, pot_pred_ID):
        if self.pred_ID is None or between(self.pred_ID, self.ID, pot_pred_ID):
            self.pred_ID = pot_pred_ID
            if self.finger_table[0] == self.ID: # Can only be the case if this is the founding node
                self.finger_table[0] = self.pred_ID


   
    '''
    components of fixing the ith finger table entry. 
    It first finds the relevant node through find_successor, 
    and then updates the the finger table with that value.
    When updating the entire finger table, these two functions are
    called m number of times.
    '''
    def fix_finger_entry_1(self, i, nodeDict):
        print('Node {} is running fix finger. i={}, so key={}'.format(self.ID, i, self.ID + 2**i))
        self.open_channel = self.ID
        self.find_successor(self.ID, self.ID + 2 ** i, nodeDict)
    def fix_finger_entry_2(self, i):
        self.finger_table[i] = self.buff


    def check_predecessor(self, nodeDict):
        try:
            nodeDict[self.pred_ID]
        except:
            self.pred_ID = None


def between(ID1, ID2, key):
    #ID1 is smaller value, #ID2 is larger value
    if ID1 == ID2:
        return True
    wrap = ID1 > ID2 #Boolean to see if wrapping occured. 
    if not wrap:
        return True if key > ID1 and key <=ID2 else False
    else:
        return True if key > ID1 or key <= ID2 else False

import math 

ring_size = 5
batch_size = 20

start_offset      = 1000
stabilize_period  = start_offset 
fix_finger_period = math.floor(start_offset*1.1)
check_pred_period = math.floor(start_offset * 0.45)

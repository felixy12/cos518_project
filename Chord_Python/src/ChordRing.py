import constants as c
import random 
import bisect

from Node import ChordNode, between
from collections import OrderedDict

class ChordRing:
    def __init__(self):
        self.nodeDict      = OrderedDict() # Dictionary of node_ID: node
        self.nodeList      = []            # Sorted list of node_IDs.
        self.curr_node_ind = 0             # Current node that is running operation.
        self.num_node      = 0             # Number of nodes in the Chord Ring. 
        
    '''
    Adds a node with specified ID. 
    '''
    def add_node(self, ID, verbose = False):
        print('Adding node: {}'.format(ID))
        if ID in self.nodeDict.keys():
            print('Node already exists on Chord. Returning')
            return
        # Initialize node, and add method of joining the chord ring.
        node = ChordNode(ID)
        if len(self.nodeDict) == 0:
            node.func_q.append((node.create, {}))
        else:
            joinID = random.choice(list(self.nodeDict.keys()))
            kwargs = {'joinID':joinID, 'nodeDict':self.nodeDict}
            node.func_q.append((node.join_1, kwargs))
            node.func_q.append((node.join_2, {}))
            node.func_q.append((node.receive_items, {'nodeDict':self.nodeDict}))
            for i in range(c.ring_size):
                node.func_q.append((node.fix_finger_entry_1, {'i': i, 'nodeDict': self.nodeDict}))
                node.func_q.append((node.fix_finger_entry_2, {'i': i}))
        # Add node to various methods of tracking node.
        self.nodeDict[ID] = node
        bisect.insort(self.nodeList, ID) # maintains sorted list
        self.num_node += 1

    '''
    Remove Node due to failure (no warning to other nodes)
    '''
    def remove_node_failure(self, ID):
        self.nodeDict.pop(ID)
        self.nodeList.remove(ID)
        self.num_node -= 1

    '''
    Perform one operation round robin style from available nodes. We do this in order
    from nodeDict so that earlier added nodes go first.
    '''
    def advance(self, verbose = False):
        # Grab relevant node.
        node_ID = list(self.nodeDict.keys())[self.curr_node_ind % self.num_node]
        node = self.nodeDict[node_ID]
        # Increase timer on node, and check if node should schedule a periodic operation.
        node.op_count += 1
        self.check_periodic_ops(node) 
        self.curr_node_ind += 1

        # If possible, grab the next op
        if len(node.func_q) == 0:
            return 
        func, kwargs = node.func_q[0]
        # If the node isn't waiting, execute.
        if node.open_channel == -1 or func in [node.return_val, node.send_batch]:
            if verbose:
                print('Node {} op count {}: Running function {}'.format(node_ID, node.op_count, func))
            node.func_q.pop(0)
            func(**kwargs)

    '''
    Adds periodic operations (stabilize, fix fingers, and check predecessor) when 
    relevant. Time is kept by local node counters.
    '''
    def check_periodic_ops(self, node):
        # There are two parts to stabilize
        if (node.op_count+node.offset) % c.stabilize_period == 0:
            print("Adding Stabilization")
            node.func_q.append((node.stabilize_1,   {'nodeDict': self.nodeDict}))
            node.func_q.append((node.stabilize_2,   {'nodeDict': self.nodeDict}))
            node.func_q.append((node.receive_items, {'nodeDict': self.nodeDict}))
        # For each finger table entry, we need to run two ops.
        if (node.op_count+node.offset) % c.fix_finger_period == 0:
            for i in range(c.ring_size):
                node.func_q.append((node.fix_finger_entry_1, {'i': i, 'nodeDict': self.nodeDict}))
                node.func_q.append((node.fix_finger_entry_2, {'i': i}))
        # For check predecessor, there is only one operation.
        if (node.op_count+node.offset) % c.check_pred_period == 0:
            node.func_q.append((node.check_predecessor, {'nodeDict': self.nodeDict}))


    '''
    Add item to Chord. Note that for the sake of this project, we aren't doing
    fancy communication to find where we should add it. 
    '''
    def add_item(self, item):
        k, v = item
        for i, curr_ID in enumerate(self.nodeList):
            prev_ID = self.nodeList[-1] 
            if between(prev_ID, curr_ID, k):
                self.nodeDict[curr_ID].storage[k] = v


    '''
    Fancy print representation of the Chord Ring.
    '''
    def __str__(self):
        to_print = ""
        for ID in self.nodeList:
            node = self.nodeDict[ID]
            to_print += 'Node {}\n'.format(ID) + '-'*40 + '\n'
            to_print += 'Predecessor: {}'.format(node.pred_ID) + '\n'
            for i, finger_ID in enumerate(node.finger_table):
                to_print += 'Finger Table Entry {}: {}'.format(i, finger_ID) + '\n'
            to_print += 'Storing keys: {}'.format(node.storage.keys()) + '\n\n'
        return to_print

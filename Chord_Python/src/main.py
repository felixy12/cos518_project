from ChordRing import ChordRing
import constants as c
import random 

verbose = True

def rand_key():
    return random.randint(0, 2 ** c.ring_size)

if __name__ == "__main__":
    chord = ChordRing()

    chord.add_node(rand_key(), verbose=True)
    for i in range(20):
        key = rand_key()
        chord.add_item((key, key))

    chord.add_node(rand_key(), verbose=True)
    #for i in range(10000):
    #    if i % 5000 == 0:
    #        chord.add_node(rand_key(), verbose=True)
    #    chord.advance(verbose=verbose)
    for j in range(20000):
        chord.advance(verbose=verbose)
    print(chord)   

# Current experiment done so far: Add one node to the ring then give it a lot of key/values.
# Then run create and join on each ring. Note that the 2nd node successfully takes over it's share of key/values.

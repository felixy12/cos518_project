package main

import (
	"fmt"
  "reflect" // this package is used to evaluate variables
  "math/rand"
  "time"
)


/* define serverNode class. When instantiated this way, each variable
 takes its default value */

type serverNode struct {
 	name int
  predecessor int
  successor int
 	values map[int]int
  requestChannel chan int 
  /* each server has one 2-buffered request channel,
  to receive request (int) and client name (int)*/
}


func findFamily(server, numServers int) (int, int) {
/* Currently based on a server's name / index and max servers,
return predecessor and successor; to be replaced with hashing-related function */
    switch server {
    case 0:
      // if first node, loop to end for predecessor
      return numServers-1, 1
    case numServers-1: 
      // if last node, loop to beginning for successor (because of zero indexing must substract 1)
      return numServers - 2, 0
    default:
      return server-1, server+1
    }
}
/* minimal function to instantiate serverNode*/
func newServerNode(name int, values map[int]int, numServers int) *serverNode {
    // create serverNode s
 	  s := serverNode{name: name, values: values}
    // must make channels to open them. this is a 2-buffered channel.
    s.requestChannel = make(chan int, 2) 
    s.predecessor, s.successor = findFamily(name, numServers)
    // return pointer to s
   	return &s
}
// function for running serverNode as goroutine that waits for input
func runServerNode(name int, values map[int]int, numServers int,
                   serverTablePointer *map[int]*serverNode,
                 clientTablePointer *map[int]*clientNode) {
    // returns pointer to s
    s := newServerNode(name, values, numServers) 
     // dereferences serverTable and inserts pointer to server under name
    (*serverTablePointer)[name] = s    
    
    // multi-line print statement acknowledging correct startup
    fmt.Printf(`server %v online, with predecessor %v, successor %v, 
    `, s.name, s.predecessor, 
    s.successor)

    // wait for request input and then return via client's channel
    for {
      // loop infinitely and wait for request on 2-buffered channel, followed by client's name
      r := <- s.requestChannel
      clientId := <- s.requestChannel
      fmt.Printf("server %v recieved request %v from client %v \n", s.name, r, clientId)

      //check if key in subset of DHT (does value exist under that key?)
      if _, ok := values[r]; ok {
        fmt.Printf("found key %v at server %v \n", r, s.name)
        // dereference clientTablePointer and find pointer to client
        c := (*clientTablePointer)[clientId]
        // send requested value and server name back to client on its 2-buffered channel
        c.responseChannel <- values[r]
        c.responseChannel <- s.name
        // if key not found at this server, pass request and client name to successor
       } else {
        successor := (*serverTablePointer)[s.successor]
            successor.requestChannel <- r
            successor.requestChannel <- clientId
       }
      
    }      
}

/* below are functions for populating Chord (i.e., starting up k servers and splitting namespace among them)
*/
func makeServerRange(numServers, server, chordUpper int) (int, int) {
/* Takes chord namespace range (assumes starts at 0) and number of 
servers, and gives start and end of range for a particular server*/
    ratio := chordUpper / (numServers - 1)
    serverStart := ratio * server
    // make sure last server doesn't extend beyond namespace
    serverEnd := newMin(ratio * (server + 1), chordUpper)
    return serverStart, serverEnd
}

func makeKeyValueSpace(serverStart, serverEnd int) map[int]int {
/* Takes server's subset of chord name space and generates partial 
k:v store randomly*/
  tempKeyValueSpace := make(map[int]int)
  for i := serverStart; i < serverEnd; i++ {
    // for all keys in server's namespace, assign random int as associated value
    tempKeyValueSpace[i] = rand.Intn(10000)
  }
  return tempKeyValueSpace
}

func populateChord(numServers int, chordUpper int,
                    serverTablePointer *map[int]*serverNode,
                    clientTablePointer *map[int]*clientNode) {
  /*Takes number of servers and range to cover with Chord,
  and starts servers as goroutines. Assumes range starts with 0 */
  for server := 0; server < numServers; server++ {
    // create namespace and populate
    serverStart, serverEnd := makeServerRange(numServers, server, chordUpper)
    tempKeyValueSpace := makeKeyValueSpace(serverStart, serverEnd)//not pointer
    // launch server
    go runServerNode(server, tempKeyValueSpace, numServers,
      serverTablePointer, clientTablePointer)
      // so serverTable isn't accessed concurrently
      time.Sleep(10 * time.Millisecond)
  }
}

// define clientNode class
type clientNode struct {
  name int
  request int
  value int
  sender int
  responseChannel chan int // each client has 2-buffered response channel
}

func newClientNode(name int, request int) *clientNode {
  // create new client node and return pointer to it
  c := clientNode{name: name, request: request}
  // response channels are 2-buffered to receive returned value and name of server
  c.responseChannel = make(chan int, 2)
  return &c  
}

func runClientRequest(name int, request int, 
  clientTablePointer *map[int]*clientNode,
  serverTablePointer *map[int]*serverNode) {
    // create new client node and return pointer to it
    c := newClientNode(name, request) 
    
    /* dereference client pointer table and insert pointer to clientNode 
    c under client name. This allows servers to pass requests around,
    and then be able to find client using just name */
    (*clientTablePointer)[name] = c

    //each client starts with random server assigned
    serverPointer := getRandomServerPointer(c.name, serverTablePointer)

    /*request protocol: send request and client name to chosen server */

    (*serverPointer).requestChannel <- c.request
    (*serverPointer).requestChannel <- c.name
    /* goroutine will wait ("block") for reply on response channel*/
    c.value = <- c.responseChannel
    c.sender = <- c.responseChannel
    fmt.Printf("recieved value %v from %v \n", c.value, c.sender)
  }

// helper functions

func newMin(x, y int) int {
  // have to code up min / max for ints every time!?!
  if x < y {
    return x
  }
  return y
 }

 func getMapRandomKey(mapI map[int]*serverNode) int {
  /* given a map, this just returns a random key. Could be faster,
  but when we scale we will likely do this another way*/
  keys := reflect.ValueOf(mapI).MapKeys() //keys of map
  return int(keys[rand.Intn(len(keys))].Int()) //random key (expecting int)
}

func getRandomServerPointer(name int, 
     serverTablePointer *map[int]*serverNode) *serverNode {
    /* will assign each client a random server to begin with*/
    // get random server name
    serverIndex := getMapRandomKey(*serverTablePointer)
    fmt.Printf("client %v initiated with access to server %v \n", name, serverIndex)
    // push through server table to get pointer to server based on server name
    serverPointer := (*serverTablePointer)[serverIndex]
    return serverPointer
    }
    
// run protocol
func main() {
  rand.Seed(time.Now().UnixNano())

  // define initial network
  numServers := 10
  // upper limit of namespace. Assumes lower limit of 0.
  chordUpper := 128
  /* state stored by application: takes node name (int) and gives pointer to 
  serverNode and clientNode strucs, respectively */
	serverTable := make(map[int]*serverNode)
  clientTable := make(map[int]*clientNode)
  populateChord(numServers, chordUpper, &serverTable, &clientTable)

  /* waits for keyboard input (hit return) after populating to ensure serverTable 
  filled properly prior to requests */
  time.Sleep(10 * time.Millisecond)
  fmt.Println("Servers populated: hit return to continue to requests")
  fmt.Scanln()
  
  clientRequests := 5
  /* create 5 clients and run requests. Currently waits for keyboard input
  after each loop (hit return) */
  for i := 0; i < clientRequests; i++ {
   go runClientRequest(i, rand.Intn(chordUpper), &clientTable, &serverTable)
   // wait for keyboard input

   fmt.Println("Hit return for next client request")
   fmt.Scanln()
  }
  // need main program to wait, unless kills all processes and quits
  fmt.Println("Hit return to exit")
  fmt.Scanln()
}

package main
import "fmt"

func hello_world() {
    fmt.Println("hello world")
}

func main() {
    for i := 0; i < 3; i++ {
        go hello_world() 
    }
    fmt.Scanln()
}

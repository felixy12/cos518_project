/* Example script of simulating 100000 nodes.
   All even nodes x send a message in their given channel.
   All odd nodes x+1 receive the message on that channel.
  
   When tested under 'salloc -N 1 -n 8' I got the following times.
   For num_nodes == 2, time elapsed is ~2 seconds
   For num_nodes == 100000, time elapsed is ~2.6 seconds
*/

package main

import (
    "fmt"
    "runtime"
    "sync"
    "time"
)

func node(process_id int, num_nodes int, message chan int, wg *sync.WaitGroup) {
    fmt.Printf("Starting process %d\n", process_id)
    defer fmt.Printf("Finished process %d\n", process_id)
    defer wg.Done()
    time.Sleep(2 * time.Second)
    
    if process_id % 2 == 0 {
      message <- process_id
    } else {
      msg := <- message
      fmt.Printf("Received message from process %d\n", msg)
    }
}

func main() {
    num_nodes := 1000000
    runtime.GOMAXPROCS(2)

    /* Print system information */
    fmt.Println("Version", runtime.Version())
    fmt.Println("NumCPU", runtime.NumCPU())
    fmt.Println("GOMAXPROCS", runtime.GOMAXPROCS(0))

    start := time.Now()

    /* Initialize coordination */
    var wg sync.WaitGroup
    wg.Add(num_nodes) 

    fmt.Println("Starting Go Routines")
    for i := 0; i < num_nodes; i+=2 {
        message := make(chan int)
        go node(i, num_nodes, message, &wg)
        go node(i+1, num_nodes, message, &wg)
    }

    fmt.Println("Waiting To Finish")
    wg.Wait()

    elapsed := time.Since(start)
    fmt.Printf("Elapsed time %s\n", elapsed)

    fmt.Println("\nTerminating Program")
}

/* Example script to spawn slow chord network.
   Network starts with a single node.
   Nodes join with randomly assigned contact node, 
   then must establish proper contact with successor and 
   predecessor.
*/

package main

import (
    "fmt";
    "time";
    "reflect";
    "math/rand"
)

type server_node struct {
  name int // hash of name
  predecessor int // hash of predecessor name
  successor int // has of successor name
  upper_limit int
  request_channel chan int 
  response_channel chan int
  /* each server has one channel,
  to receive commands from predecessor*/
}

func (s *server_node) key_match(key int) bool {
	// if seed node: this case is just needed for initialization
  if (s.predecessor == s.name) && (s.successor == s.name) {
    return true
  } else if (key >= s.predecessor) && (key < s.name) {
  	// normal case
  	return true
    } else if (key + s.upper_limit > s.predecessor) && (key < s.name) {
    	//wrap around case if k after 0
		return true
	}  else if (key >= s.predecessor) && (key > s.name) {
    	//wrap around case if k before 0
		return true
    } else {
    	fmt.Println("key not found here")
    	return false
    }
}

func (s *server_node) find_successor(server_pointer_table *map[int]*server_node) {
  p := &(*server_pointer_table)[s.successor].request_channel
  q := &(*server_pointer_table)[s.successor].response_channel
  *p <- 1
  *p <- s.name
  fmt.Println("find successor request sent to successor channel")
  msg := <- *q
  if msg == 2 {
    new_successor := <- *q
    s.successor = new_successor
    fmt.Printf("new successor set for node %v: %v \n", s.name, s.successor)
  } else {
    fmt.Println("not implemented yet: bad response to find_successor")
  }
}

func (s *server_node) find_predecessor(server_pointer_table *map[int]*server_node) {
  p := &(*server_pointer_table)[s.successor].request_channel
  q := &(*server_pointer_table)[s.successor].response_channel
  *p <- 3
  *p <- s.name
  fmt.Println("find predecessor sent to successor channel")
  msg := <- *q
  if msg == 4 {
    new_predecessor := <- *q
    s.predecessor = new_predecessor
    fmt.Printf("new predecessor set for node %v: %v \n", s.name, s.predecessor)
    (*server_pointer_table)[s.predecessor].request_channel <- 5
    (*server_pointer_table)[s.predecessor].request_channel <- s.name
    fmt.Printf("new successor of %v sent to node %v \n", s.name, s.predecessor)
  } else {
    fmt.Println("not implemented yet: bad response to find_successor")
  }
}

func create_server_node(process_id int, successor int, chord_upper int,
                       server_pointer_table *map[int]*server_node,
                       free_name_table_pointer *map[int]int) *server_node {
    s := server_node{name: process_id, successor: successor, upper_limit: chord_upper}
    s.request_channel = make(chan int, 2)
    s.response_channel = make(chan int, 2)
    delete(*free_name_table_pointer, s.name)
    (*server_pointer_table)[s.name] = &s
    return &s
}

func node_protocol(s *server_node, 
           server_pointer_table *map[int]*server_node) {
    /* messsage from predecessor */
    fmt.Printf("node %v waiting for input \n", s.name)
    for { 
      msg := <- s.request_channel
      val := <- s.request_channel
      switch msg {
      case 1:
        fmt.Printf("key %v lookup received by server %v \n", val, s.name)
        if s.key_match(val) {
          fmt.Printf("key %v matched by server %v \n", val, s.name)
          s.response_channel <- 2
          s.response_channel <- s.name
        } else {
          fmt.Printf("forwarding request for key %v to server %v \n", val, s.successor)
          (*server_pointer_table)[s.successor].request_channel <- 1
          (*server_pointer_table)[s.successor].request_channel <- val
          fmt.Printf("forwarded request for key %v to server %v \n", val, s.successor)
          temp1 := <- (*server_pointer_table)[s.successor].response_channel
          temp2 := <- (*server_pointer_table)[s.successor].response_channel
          fmt.Printf("received response for key %v from server %v \n", val, temp2)
          s.response_channel <- temp1
          s.response_channel <- temp2
          fmt.Printf("passed message back from server %v \n", temp2)
        }
      case 3: 
        fmt.Printf("predecessor lookup received by server %v \n", s.name)
        s.response_channel <- 4
        s.response_channel <- s.predecessor
        s.predecessor = val
        fmt.Printf("node %v has new predecessor: %v \n", s.name, s.predecessor)
      case 5: 
        fmt.Printf("set successor received by server %v \n", s.name)
        s.successor = val
        fmt.Printf("node %v has new successor: %v \n", s.name, s.successor)
      }
    }
}


func go_seed_node(process_id int, chord_upper int,
               server_pointer_table *map[int]*server_node,
               free_name_table_pointer *map[int]int) {
     s := create_server_node(process_id, process_id, chord_upper, 
     	                     server_pointer_table, free_name_table_pointer)
     s.successor = s.name
     s.predecessor = s.name
     fmt.Println("seed node created with following details")
     fmt.Printf("%+v\n", s) // Print with Variable Name
     node_protocol(s, server_pointer_table)
}

func go_server_node(process_id int, assigned_server int, chord_upper int,
     server_pointer_table *map[int]*server_node,
     free_name_table_pointer *map[int]int) {
     s := create_server_node(process_id, assigned_server, chord_upper,
     					     server_pointer_table, free_name_table_pointer)
     fmt.Println("server node created with following details")
     fmt.Printf("%+v\n", s) // Print with Variable Name
     s.find_successor(server_pointer_table)
     s.find_predecessor(server_pointer_table)
     node_protocol(s, server_pointer_table)
}

func get_random_server(map_i map[int]*server_node) int {
  /* given a map, this just returns a random key. Could be faster,
  but when we scale we will likely do this another way*/
  keys := reflect.ValueOf(map_i).MapKeys() //keys of map
  return int(keys[rand.Intn(len(keys))].Int()) //random key (expecting int)
}

func get_random_name(map_i map[int]int) int {
  /* given a map, this just returns a random key. Could be faster,
  but when we scale we will likely do this another way*/
  keys := reflect.ValueOf(map_i).MapKeys() //keys of map
  return int(keys[rand.Intn(len(keys))].Int()) //random key (expecting int)
}

//server_index := get_map_random_key(*serverTablePointer)
func make_free_name_table(upper_limit int) map[int]int {
     map_k := make(map[int]int)
     for i := 0; i < upper_limit; i++ {
         map_k[i] = i}
     return map_k
}

func main() {
	fmt.Println("\n")
    chord_upper := 128
    fmt.Println("Initializing first node")
    server_table := make(map[int]*server_node)
    free_name_table := make_free_name_table(chord_upper)
    seed_id := get_random_name(free_name_table)

    go go_seed_node(seed_id, chord_upper, &server_table, &free_name_table)

    time.Sleep(10 * time.Millisecond)
    fmt.Scanln()
    
    for i := 0; i < 5; i++ {
      process_id := get_random_name(free_name_table)
      initial_contact := get_random_server(server_table)
      go go_server_node(process_id, initial_contact, chord_upper, &server_table, &free_name_table)
      fmt.Println("\n")
      time.Sleep(10 * time.Millisecond)
      fmt.Scanln()
    }

    time.Sleep(10 * time.Millisecond)
    fmt.Scanln()
    fmt.Println("\nTerminating Program")
}

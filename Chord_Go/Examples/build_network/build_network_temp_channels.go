/* Example script to spawn slow chord network.
   The main idea here is that servers use methods to create
   temporary channels for communication. 
   Servers locate the IP address, and therefore methods, of successors
   and predecessors by using a shared lookup table.
   servers run as goroutines and block for kill and input messages

   First, a server_node type is defined and several methods for
   pointers to this type, including initialization methods, 
   find successor and predecessor methods, and a node protocol.

   Then, a function to create such nodes and return pointers.

   Then, functions to populate chord and helper functions.

   Finally, the main run script. I recommend starting here!
*/

package main

import (
    "fmt";
    "time";
    "reflect";
    "math/rand"
)

type server_node struct {
  name int // hash of name
  predecessor int // hash of predecessor name
  successor int // has of successor name
  kill_channel chan int
  client_channel chan int
  /* each server has one channel,
  to receive commands from predecessor*/
}

func (s *server_node) return_predecessor() int {
  return s.predecessor
  }

func (s *server_node) set_predecessor(name int) {
  s.predecessor = name
}

func (s *server_node) return_successor() int {
  return s.successor
  }

func (s *server_node) set_successor(name int) {
  s.successor = name
}

func (s *server_node) key_match(key int) bool {
  //fmt.Printf("key lookup for %v received by %v. \n", key, s.name)
	// if seed node: this case is just needed for initialization
  if (s.predecessor == s.name) && (s.successor == s.name) {
    //fmt.Println("case 1 \n")
    return true
  } else if (key >= s.predecessor) && (key < s.name) {
    //fmt.Println("case 2 \n")
  	// normal case
  	return true
    } else if (s.name < s.predecessor) && (key < s.name) {
      //fmt.Println("case 3 \n")
    	//wrap around case if k after 0
		return true
	}  else if (s.name < s.predecessor) && (key >= s.predecessor) {
      //fmt.Println("case 4 \n")
    	//wrap around case if k before 0
		return true
    } else { 
      // key not found in server node's chord range
      // fmt.Println("case 5 (fail) \n")

    	return false
    }
}

func (s *server_node) send_key_lookup(key int, target_node int, 
  server_pointer_table *map[int]*server_node) int {
    /* this method creates a channel for a key lookup,
    loads it with the key, and sends it to successor via
    server_pointer_table (frontend state with name:IP address mappings.*/

  // make request channel and load with key
  //fmt.Println("making request channel")
  request_channel := make(chan int, 1)
  //fmt.Println("made channel")
  request_channel <- key
  //fmt.Println("requests sent: ", key)
  // send pointer to channel to successor via receive_key_lookup method; response channel pointer returned
  response_channel := (*server_pointer_table)[s.successor].receive_key_lookup(&request_channel, 
                                                                              server_pointer_table)
  // block on receiving response
  response := <- *response_channel
  // fmt.Printf("responses received: %v \n", response)

  // close channels
  close(request_channel)
  close(*response_channel)
  return response
  }

func (s *server_node) receive_key_lookup(request_channel *chan int,
  server_pointer_table *map[int]*server_node) *chan int {
  /* this method receives a key lookup, along with pointer to channel.
  Creates response channel and returns pointer to it to querying server*/

  // make new temporary response channel
  response_channel := make(chan int, 1)
  // receive key
  key := <- *request_channel
  //fmt.Printf("key lookup for %v received at node with following details", key)
  //fmt.Printf("%+v\n", s) // Print with Variable Name
  
  // check if match
  match := s.key_match(key)
  // main section to extend
  if match { // load response channel with name if key found in server's namespace
    response_channel <- s.name
  } else { // otherwise, forward key lookup to successor
    //fmt.Println("forwarding request to successor")

    //creaete new request channel
    request_channel_2 := make(chan int, 1)
    request_channel_2 <- key // load with key that failed lookup
    //fmt.Printf("sending request for %v to %v \n", key, s.successor)

    // send pointer to that channel to successor and receive pointer to its response channel
    response_channel_2 := (*server_pointer_table)[s.successor].receive_key_lookup(&request_channel_2, server_pointer_table)
    // block on response from server (may propagate further along ring)
    response := <- *response_channel_2 
    response_channel <- response // return returned name to original server via primary response chnanel

    // close all new temporary channels
    close(request_channel_2)
    close(*response_channel_2)
  }
  // return pointer to loaded channel
  //fmt.Println("responses sent: ", match, s.name)
  return &response_channel
  }

func (s *server_node) find_successor(server_pointer_table *map[int]*server_node) {
  /* This method triggers initial find successor call*/
  fmt.Println("sending initial successor lookup")

  // we know successor should own key with same value as name of server
  s.successor = s.send_key_lookup(s.name, s.successor, server_pointer_table)
  
  // a bit hacky---uses frontend to find IP of new successor's old predecessor;
  //Sets old successor's predeccesor as own
  s.predecessor = (*server_pointer_table)[s.successor].return_predecessor()
  
  // adds itself as old successor's predeccesor's new successor to close gap
  (*server_pointer_table)[(*server_pointer_table)[s.successor].predecessor].successor = s.name
  
  //resets new successors predecessor as itself to close gap
  (*server_pointer_table)[s.successor].predecessor = s.name
  
  fmt.Println("\n server node has updated details")
  fmt.Printf("%+v\n", s) // Print with Variable Name
  fmt.Println("\n successor node has updated details")
  fmt.Printf("%+v\n", (*server_pointer_table)[s.successor]) // Print with Variable Name
  fmt.Println("\n predecessor node has updated details")
  fmt.Printf("%+v\n", (*server_pointer_table)[s.predecessor]) // Print with Variable Name

}

func (s server_node) node_protocol() {
    /* wait for kill or request messsage from frontend */
    fmt.Printf("node %v waiting for input \n", s.name)
    for { select {
      case <- s.kill_channel:
        fmt.Println("kill request received")
        return
      case <- s.client_channel:
        fmt.Println("client request received")
        return

      }
  }
}

func create_server_node(process_id int, successor int,
                       server_pointer_table *map[int]*server_node,
                       free_name_table_pointer *map[int]int) *server_node {
    s := server_node{name: process_id, successor: successor}
    delete(*free_name_table_pointer, s.name) // remove name from free name table
    s.kill_channel = make(chan int)
    s.client_channel = make(chan int)
    (*server_pointer_table)[s.name] = &s // add IP address (here done with pointer) to frontend
    return &s
}


func go_seed_node(process_id int, 
               server_pointer_table *map[int]*server_node,
               free_name_table_pointer *map[int]int) {
      /* initial node is unique in that its successor and 
                predecessor are initially set to itself. This is crucial
                for correct initial key lookup for the first node that joins after*/
     s := create_server_node(process_id, process_id,
     	                     server_pointer_table, free_name_table_pointer)
     s.predecessor = s.name
     fmt.Println("seed node created with following details")
     fmt.Printf("%+v\n", s) // Print with Variable Name
     s.node_protocol() // enter blocking response loop
}

func go_server_node(process_id int, assigned_server int,
     server_pointer_table *map[int]*server_node,
     free_name_table_pointer *map[int]int) {
      /* As above, but has initial successor randomly assigned in main()*/
     s := create_server_node(process_id, assigned_server,
     					     server_pointer_table, free_name_table_pointer)
     fmt.Println("server node created with following details")
     fmt.Printf("%+v\n", s) // Print with Variable Name
     s.find_successor(server_pointer_table) // begin with find_successor call (will give predecessor automatically)
     s.node_protocol() // enter blocking response loop
}
func get_random_server(map_i map[int]*server_node) int {
  /* given a map, this just returns a random key. Could be faster,
  but when we scale we will likely do this another way
  Defined for special map output*/
  keys := reflect.ValueOf(map_i).MapKeys() //keys of map
  return int(keys[rand.Intn(len(keys))].Int()) //random key (expecting int)
}

func get_random_name(map_i map[int]int) int {
  /* given a map, this just returns a random key. Could be faster,
  but when we scale we will likely do this another way.
  Defined for int output*/
  keys := reflect.ValueOf(map_i).MapKeys() //keys of map
  return int(keys[rand.Intn(len(keys))].Int()) //random key (expecting int)
}

func make_free_name_table(upper_limit int) map[int]int {
     /* seeds free name map with all available values.
     These are then removed by joining nodes.*/
     map_k := make(map[int]int)
     for i := 0; i < upper_limit; i++ {
         map_k[i] = i}
     return map_k
}
func main() {
  /* Main coordinator loop. See comments below for how to break. */
	fmt.Println("\n")
    chord_upper := 128 //  working with namespace of integers under this value, currently
    num_nodes := 10 // number of nodes to initialize with
    fmt.Println("Initializing first node")
    server_table := make(map[int]*server_node)
    free_name_table := make_free_name_table(chord_upper)
    seed_id := get_random_name(free_name_table)

    // launch initial node
    go go_seed_node(seed_id, &server_table, &free_name_table)

    time.Sleep(10 * time.Millisecond)
    fmt.Println("Hit return to add nodes")
    fmt.Scanln()
    
    // create and add nodes to chord. BREAK: If delay commented out, program crashes.
    for i := 0; i < num_nodes; i++ {
      fmt.Printf("\n %vth NODE JOINS!\n", i)
      process_id := get_random_name(free_name_table) // get free name
      initial_contact := get_random_server(server_table) // get initial contact
      go go_server_node(process_id, initial_contact, 
                        &server_table, &free_name_table)
      fmt.Println("\n")

      // comment out below to BREAK
      time.Sleep(1 * time.Millisecond)
      //fmt.Scanln()
    }

    time.Sleep(10 * time.Millisecond)
    fmt.Println("Hit return to exit")
    fmt.Scanln()
    fmt.Println("\nTerminating Program")
}

/* Example script to spawn slow chord network.
   The main idea here is that servers have two ports---
   incoming ("request channel") and outgoing ("response channel").
   Servers locate the IP address, and therefore ports, of successors
   and predecessors by using a shared lookup table.

   First, a server_node type is defined and several methods for
   pointers to this type.

   Then, a function to create such nodes and return pointers.

   Then, the node protocol is defined: behaviour while waiting for 
   input.

   Then, functions to populate chord and helper functions.

   Finally, the main run script. I recommend starting here!
*/

package main

import (
    "fmt";
    "time";
    "reflect";
    "math/rand"
)

type server_node struct {
  name int // hash of name
  predecessor int // hash of predecessor name
  successor int // has of successor name
  request_channel chan int // incoming messages channel
  response_channel chan int // outgoing messages channel
}

func (s *server_node) key_match(key int) bool {
  //fmt.Printf("key lookup for %v received by %v. \n", key, s.name)
	// if seed node: this case is just needed for initialization
  if (s.predecessor == s.name) && (s.successor == s.name) {
    //fmt.Println("case 1 \n")
    return true
  } else if (key >= s.predecessor) && (key < s.name) {
    //fmt.Println("case 2 \n")
  	// normal case
  	return true
    } else if (s.name < s.predecessor) && (key < s.name) {
      //fmt.Println("case 3 \n")
    	//wrap around case if k after 0
		return true
	}  else if (s.name < s.predecessor) && (key >= s.predecessor) {
      //fmt.Println("case 4 \n")
    	//wrap around case if k before 0
		return true
    } else {
      // key not found in server node's chord range
    	//fmt.Println("key not found here")
    	return false
    }
}

func (s *server_node) find_successor(server_pointer_table *map[int]*server_node) {
  /* this method for the server node type sends a successor request 
  via initially assigned server's ports
  */
  // define pointers to ports for shorthand
  p := &(*server_pointer_table)[s.successor].request_channel 
  q := &(*server_pointer_table)[s.successor].response_channel
  *p <- 1 // send symbol for successor request
  *p <- s.name // we know this key will be owned by succeessor

  //fmt.Println("find successor request sent to successor channel")
  // block on receiving response
  msg := <- *q
  if msg == 2 { // if message is "return successor"
    new_successor := <- *q
    s.successor = new_successor // set successor to name
    fmt.Printf("new successor set for node %v: %v \n", s.name, s.successor)
  } else {
    fmt.Println("not implemented yet: bad response to find_successor")
  }
}

func (s *server_node) find_predecessor(server_pointer_table *map[int]*server_node) {
  /* this method is just for initial setup; former predecessor request sent to successor 
  via initially assigned server's ports
  */
  // define pointers to ports for shorthand
  p := &(*server_pointer_table)[s.successor].request_channel
  q := &(*server_pointer_table)[s.successor].response_channel
  *p <- 3 // send find_predecessor request
  *p <- s.name // send name of requesting server
  //fmt.Println("find predecessor sent to successor channel")
  msg := <- *q
  if msg == 4 { // if successor replies correctly
    new_predecessor := <- *q
    s.predecessor = new_predecessor // set new predecessor for server
    fmt.Printf("new predecessor set for node %v: %v \n", s.name, s.predecessor)
    (*server_pointer_table)[s.predecessor].request_channel <- 5 // update former predecessor request
    (*server_pointer_table)[s.predecessor].request_channel <- s.name // send server name as new successor
    //fmt.Printf("new successor of %v sent to node %v \n", s.name, s.predecessor)
  } else {
    fmt.Println("not implemented yet: bad response to find_successor")
  }
}

func create_server_node(process_id int, successor int,
                       server_pointer_table *map[int]*server_node,
                       free_name_table_pointer *map[int]int) *server_node {
                        /* create new instance of type server node.
                        Have to make channels this way. Update central
                        state (add IP to frontend and remove name from 
                        availble names) */
    s := server_node{name: process_id, successor: successor}
    s.request_channel = make(chan int, 2)
    s.response_channel = make(chan int, 2)
    delete(*free_name_table_pointer, s.name)
    (*server_pointer_table)[s.name] = &s
    return &s
}

func node_protocol(s *server_node, 
           server_pointer_table *map[int]*server_node) {
    /*  Primary response loop---block until input */
    //fmt.Printf("node %v waiting for input \n", s.name)
    for { // loop indefinitely
      msg := <- s.request_channel //block on receiving on input port 
      val := <- s.request_channel
      switch msg { // first term through channel will give type of message
      case 1: // a key lookup
        //fmt.Printf("key %v lookup received by server %v \n", val, s.name)
        if s.key_match(val) { // if server contains key
          //fmt.Printf("key %v matched by server %v \n", val, s.name)
          s.response_channel <- 2 // send back positive result directly
          s.response_channel <- s.name // server identifies itself
        } else { // forward request to successor
          //fmt.Printf("forwarding request for key %v to server %v \n", val, s.successor)
          (*server_pointer_table)[s.successor].request_channel <- 1 // key lookup
          (*server_pointer_table)[s.successor].request_channel <- val // received value forwarded
          //fmt.Printf("forwarded request for key %v to server %v \n", val, s.successor)

          //block on response from successor (should be positive key lookup, and final server name)
          temp1 := <- (*server_pointer_table)[s.successor].response_channel // on successor's outgoing port
          temp2 := <- (*server_pointer_table)[s.successor].response_channel // successful server name
          //fmt.Printf("received response for key %v from server %v \n", val, temp2)

          // send responses back to predecessor via outgoing port
          s.response_channel <- temp1
          s.response_channel <- temp2
          //fmt.Printf("passed message back from server %v \n", temp2)
        }
      case 3: // predecessor lookup
        //fmt.Printf("predecessor lookup received by server %v \n", s.name)
        s.response_channel <- 4 // send back confirmation
        s.response_channel <- s.predecessor // send back old predecessor
        s.predecessor = val // set new predecessor
        fmt.Printf("node %v has new predecessor: %v \n", s.name, s.predecessor)
      case 5: // set succeessor request: if node has joined ahead
        //fmt.Printf("set successor received by server %v \n", s.name)
        s.successor = val
        fmt.Printf("node %v has new successor: %v \n", s.name, s.successor)
      }
    }
}


func go_seed_node(process_id int, chord_upper int,
               server_pointer_table *map[int]*server_node,
               free_name_table_pointer *map[int]int) {
                /* initial node is unique in that its successor and 
                predecessor are initially set to itself. This is crucial
                for correct initial key lookup for the first node that joins after*/
     s := create_server_node(process_id, process_id,
     	                     server_pointer_table, free_name_table_pointer)
     s.successor = s.name
     s.predecessor = s.name
     fmt.Println("seed node created with following details")
     fmt.Printf("%+v\n", s) // Print with Variable Name
     node_protocol(s, server_pointer_table) // enter blocking response loop
}

func go_server_node(process_id int, assigned_server int, chord_upper int,
     server_pointer_table *map[int]*server_node,
     free_name_table_pointer *map[int]int) {
      /* As above, but has initial successor randomly assigned*/
     s := create_server_node(process_id, assigned_server,
     					     server_pointer_table, free_name_table_pointer)
     fmt.Println("server node created with following details")
     fmt.Printf("%+v\n", s) // Print with Variable Name
     s.find_successor(server_pointer_table) // initialized with a find successor call
     s.find_predecessor(server_pointer_table) // reset new successors predecessor and find own
     node_protocol(s, server_pointer_table) // enter blocking response loop
}

func get_random_server(map_i map[int]*server_node) int {
  /* given a map, this just returns a random key. Could be faster,
  but when we scale we will likely do this another way
  Defined for special map output*/
  keys := reflect.ValueOf(map_i).MapKeys() //keys of map
  return int(keys[rand.Intn(len(keys))].Int()) //random key (expecting int)
}

func get_random_name(map_i map[int]int) int {
  /* given a map, this just returns a random key. Could be faster,
  but when we scale we will likely do this another way.
  Defined for int output*/
  keys := reflect.ValueOf(map_i).MapKeys() //keys of map
  return int(keys[rand.Intn(len(keys))].Int()) //random key (expecting int)
}

func make_free_name_table(upper_limit int) map[int]int {
     /* seeds free name map with all available values.
     These are then removed by joining nodes.*/
     map_k := make(map[int]int)
     for i := 0; i < upper_limit; i++ {
         map_k[i] = i}
     return map_k
}

func main() {
  /* Main coordinator loop. See comments below for how to break. */
	fmt.Println("\n")
    chord_upper := 128 //  working with namespace of integers under this value, currently
    num_nodes := 10 // number of nodes to initialize with
    fmt.Println("Initializing first node")
    server_table := make(map[int]*server_node) // maps names to IPs: central state held by frontend
    free_name_table := make_free_name_table(chord_upper) //map of free names; initially all availble integers 
    seed_id := get_random_name(free_name_table) // initial node gets random name

    // launch initial node
    go go_seed_node(seed_id, chord_upper, &server_table, &free_name_table)

    time.Sleep(10 * time.Millisecond)
    fmt.Println("Hit return to add nodes")
    fmt.Scanln()
     
    // create and add nodes to chord. BREAK: If delay commented out, program crashes.
    for i := 0; i < num_nodes; i++ {
      fmt.Println("\n NODE JOINS!")
      process_id := get_random_name(free_name_table) // get free name
      initial_contact := get_random_server(server_table) // get initial contact

      // launch nodee
      go go_server_node(process_id, initial_contact, chord_upper, 
                                  &server_table, &free_name_table)
      fmt.Println("\n")
      // comment out beelow to BREAK
      time.Sleep(10 * time.Millisecond)
      //fmt.Scanln()
    }

    time.Sleep(10 * time.Millisecond)
    fmt.Println("Hit return to exit")
    fmt.Scanln()
    fmt.Println("\nTerminating Program")
}

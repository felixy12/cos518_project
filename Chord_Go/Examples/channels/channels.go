/* Simple example of how to use go channels */

package main

import "fmt"

func foo(messages chan string) {
  messages <- "ping"
}

func main() {

  messages := make(chan string)
  go foo(messages)

  msg := <- messages
  fmt.Println(msg)
}

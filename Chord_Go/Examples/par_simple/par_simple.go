/* Simple example of concurrency vs parallelism in Go.
   GOMAXPROCS(n) sets the number of OS processes to be used. By default it is equal to the number
   of CPU cores, call it q.

   Calling GOMAXPROCS(n) uses at most n CPU cores. Examining with htop confirms this.
   Calling GOMAXPROCS(1) means 100% concurrency, zero parallelism.
   Calling GOMAXPROCS(n) with n > q causes Go to mimic n OS processes using only q cores.
   I noticed that this causes performance to degrade.

   It seems that using the default settings for GOMAXPROCS and letting Go use its concurrency scheduler
   results in the best simulation environment.
*/

package main

import (
    "fmt"
    "runtime"
    "sync"
)

func main() {
    runtime.GOMAXPROCS(100)  // <-- Play with this number

    /* Print system information */
    fmt.Println("Version", runtime.Version())
    fmt.Println("NumCPU", runtime.NumCPU())
    fmt.Println("GOMAXPROCS", runtime.GOMAXPROCS(0))

    /* Initialize coordination */
    var wg sync.WaitGroup
    wg.Add(3) // 3 is the number of goroutines to be run

    fmt.Println("Starting Go Routines")
    go func() {
        defer wg.Done()
        for char := 1; char < 200000; char++ {
            fmt.Printf("%c ", 'a')
        }
    }()

    go func() {
        defer wg.Done()
        for number := 1; number < 200000; number++ {
            fmt.Printf("%d ", number)
        }
    }()

    go func() {
        defer wg.Done()
        for number := 1; number < 200000; number++ {
            fmt.Printf("%d ", -number)
        }
    }()

    fmt.Println("Waiting To Finish")
    wg.Wait()

    fmt.Println("\nTerminating Program")
}

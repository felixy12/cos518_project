# Implementation of Chord in Golang using gRPC
This directory contains the implementation of Chord in Golang for the fall 2019 project of COS518 for Ruri Battleday, Ethan Tseng, and Felix Yu. Message passing between nodes is handled using gRPC.

## Setting up Environment 
### Installing Go
Go can be installed through instructions found [here](https://golang.org/doc/install). Follow the guide and make sure the installed version is higher than 1.6 for gRPC compatibility. 

### Installing gRPC
Installing gRPC can be done by following the instructions [here](grpc.io/docs/quickstart/go/). Here are steps I took to get gRPC running. Before following the guide, make sure you know what your GOPATH and GOROOT are. You can do this with `go env` and looking for those variables.  
1. Install grpc with `go get -u google.golang.org/grpc`. This will silently install grpc in your GOPATH.  
2. Install Protocol Buffers v3 by going [here](https://github.com/google/protobuf/releases) and downloading `protobuf-all-3.11.1.zip`. Unzip, and then update environment variable with `export PATH=$PATH:/<PATHDIR>/protobuf-3.11.1`.  
3. Install protoc plugin with `go get -u github.com/golang/protobuf/protoc-gen-go`. This will silently install protoc-gen-go into GOPATH.  
4. Copy protoc-gen-go file, found in `$GOPATH/bin/` into your `$GOROOT/bin`. If your `GOPATH` and `GOROOT` are the same, skip this step.  
5. To test if everything is working, run through the example specified in the installation instructions.   

### Recognize Chord packages
During the course of this implementation, we want Go to recognize packages that we define in this project directory. One way to do so is to import the packages from a public Github repo. However, this requires constantly pushing updates onto the repo in order to access changes. Instead, we can do the following: run `pwd` to get the current directory (should end with `Chord_Go/Implementation`), and then in your `.bash_profile`, include the following line: `export GOPATH=<CURR GO PATH>:<CURR DIR>`. Now, Go will look in the `src` directory for packages. Use `go env` to make sure the path was sucecssfully appended.  
To test if the path is running correctly, you can call `go run run/helloworld.go`. It should print `Hello World` three times, and then you can press any key to exit.

## Directory Structure
.  
+-- src  
|   +-- chord  
|   +-- chordnode\_proto  
+-- run  

`src` contains all source code, and is where our packages are defined.  
`chord` contains all `.go` files which implement Chord in Go.  
`chordnode_proto` contains the `.proto` file and the compiled `.pb.go` file used to define RPCs.  
`run` contains all code that can be run by Go to launch Chord and go through various experiments.  

package chord

import (
    _ "fmt"
    _ "google.golang.org/grpc"
    _ pb "chordnode_proto"
)    

const (
    bits_in_key = 8
    stabilizeInterval = 50
    fixFingerInterval = 50
)

type ChordNodeServer struct {
    // ID of the node
    nKey pb.nodeKey
    key int
    
    // array of node IDs, used to construct finger tables.
    fingerTable []*pb.nodeKey

    // ID of the node's predecessor
    predecessor pb.nodeKey 

    // List of all keys currently stored by node.
    storage []*pb.itemKey

    // Server used for gRPC communication 
    grpcServer = grpc.NewServer()
}

func Setup(key int, startingNodeKey int) *ChordNodeServer {
    node := &ChordNodeServer{
        nKey: new(pb.nodeKey)
        key: key 
        fingerTable: make([]*pb.nodeKey, bits_in_key) 
        predecessor: nil
        storage: make([]*pb.itemKey) 
    }
    // set up grpc server stuff.
    grpcServer = grpc.NewServer()
    pb.RegisterChordServer(node.grpcServer, node)
    node.grpcServer.Serve(1) // TODO: FIND OUT WHAT THIS MEANS LATER 

    node.join(&pb.nodeKey{key: startingNodeKey})

    return node
}

func (node *ChordNodeServer) join(startingNodeKey *pb.nodeKey) {
    // If this is the first node, we do nothing.
    if startingNodeKey.key == -1 {
        return
    }    
    successor := node.findSuccessor(startingNodeKey, node.key)
    node.fingerTable[0] = successor
    return
}

func (node *ChordNodeServer) findSuccessor(key *pb.nodeKey) 
